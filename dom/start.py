#!/usr/bin/env python
# -*- coding: utf-8 -*-
from wsgiref.simple_server import make_server
from cgi import parse_qs, escape
import bookdb

titlesHtml = []
books = bookdb.BookDB()
#append titles to the list
for title in books.titles():
	titlesHtml.append("<li>"+"<a href=""http://194.29.175.240:31014/?id="+title["id"]+"&"">"+title['title']+"</a>"+"</li>")

body = """
	<html>
	<body>
		<p>
			Lista ksiazek:
		<p>
		<ol>
		%s
		</ol>
	</body>
	</html>"""

html = """
	<html>
	<body>
		<p>Tytul: %s</p>
		<p>ISBN: %s</p>
		<p>Wydawca: %s</p>
		<p>Autor: %s</p>
		<a href="http://194.29.175.240:31014/">Powrot do listy ksiazek</a>
	</body>
	</html>
	"""

def server(environ, start_response):
	dict = parse_qs(environ['QUERY_STRING'])

	idBook = dict.get('id', [''])[0] # Returns the first book id.

	idBook = escape(idBook)
	if idBook is '':
		response_body = body % ( "".join(titlesHtml))
	else:
		response_body = html % (books.title_info(idBook)['title'],
	books.title_info(idBook)['isbn'],
	books.title_info(idBook)['publisher'],
	books.title_info(idBook)['author'])
	status = '200 OK'

	response_headers = [('Content-Type', 'text/html'),
	('Content-Length', str(len(response_body)))]
	start_response(status, response_headers)
	return [response_body]

if __name__ == '__main__':
	httpd = make_server('194.29.175.240', 31014, server)
	httpd.serve_forever()
